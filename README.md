#### Purpose

This script will allow you to spin up a controller node on DigitalOcean that will be used to control your infrastructure. The initial cloud-config script wiill set up all dependencies for Terraform and Ansible to function properly as well as download and set up a simple configuration file generator.

#### Setting up

Log into your DigitalOcean account in order to gather a few items. You'll want to follow the steps listed [here](https://www.digitalocean.com/community/tutorials/how-to-use-the-digitalocean-api-v2#how-to-generate-a-personal-access-token) to create an API token.

Execute the cloud-config script through user-data when you create a Droplet in the control panel. Wait about one additional minute for the script to complete all installations.

Log into the new Droplet and execute `terragen`. You'll be prompted for your API token and few more variables. 
